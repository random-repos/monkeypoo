import sys
import pyglet
import time
import primitives
import pygame
import random
from pyglet import window
from datatypes import *


MONKEY_LENGTH = 1000
FREQ = 7
SPEED = 7
STICK_SPEED = .4
WIND_SPEED = .5
CIRCLE_SIZE = 50
def pointsinbox(l,x1,x2,y1,y2):
    indices = list()
    for i in range(len(l)-1):
        p = l[i]
        if p.x < x1 or p.x > x2 or p.y < y1 or p.y > y2:
            continue
        indices.append(i)
    return indices
class Camera:
    #this camera class should support rotation and scaling
    def __init__(self,window):
        self.window = window
        self.width = window.width
        self.height = window.height
        self.pos = Vector2d(0,0)
        self.rot = 0
        self.scale = 1
    def setPos(self,pos):
        self.pos = Vector2d(pos.x,pos.y)
    def setRot(self,v):
        """sets rotation based on a vector assumed to be centerd at self.pos"""
        self.rot = v.getAngle() + math.pi/2
        #print self.rot
    def toRot(self,v):
        #TODO this needs to compensate for the rollover on angles
        r = v.getAngle()+math.pi
        if self.rot - r > math.pi:
            r+= math.pi*2
        elif r-self.rot > math.pi:
            r-= math.pi*2
        self.rot = self.rot*.95 + (r)*.05
        if self.rot > math.pi * 2:
            self.rot -= math.pi*2
    def getRot(self):
        return getVector2dFromPolar(1,self.rot)
    def setScale(self,s):
        self.scale = s
    def convertPoint(self,point):
        #translate 2 center
        v = point-self.pos
        a = self.rot
        #rotate
        v.x,v.y = v.x*math.cos(a)-v.y*math.sin(a),v.x*math.sin(a)+v.y*math.cos(a)
        v *= self.scale
        return v
    def center(self,point):
        return Vector2d(point.x + self.width/2, point.y + self.height/2)
    def pointsonscreen(self,points,radius):
        pass

class Circles():
    def __init__(self):
        self.prim = primitives.Circle(0,0,0,0,(0,0,0,1))
        self.increment = 300
        self.number = 10
        self.pos = Vector2d(0,0)
    def draw(self,camera):
        l = camera.convertPoint(self.pos)
        l = camera.center(l)
        self.prim.setLoc((l.x,l.y))
        for i in range(self.number):
            self.prim.setWidth(((self.number - i)*self.increment)*camera.scale)
            self.prim.color = (0,((float(i)/self.number)),0,255)
            #lol this is cool
            #self.prim.color = (random.randint(0,255),random.randint(0,255),random.randint(0,255),1)
            self.prim.render()

class Win( window.Window ):
    monkey = list()
    move = Vector2d(1,0)
    desired = Vector2d(1,0)
    def __init__(self,*args, **kwargs):
        #init pyglet
        window.Window.__init__(self, *args, **kwargs)
        pyglet.clock.set_fps_limit(30)
        pyglet.clock.schedule(self.update)
        #init pygame joystick stuff
        pygame.init()
        self.joy = None
        if pygame.joystick.get_count() > 0:
            self.joy = pygame.joystick.Joystick(0)
            self.joy.init()
            self.joyv = Vector2d(0,0)
        #init camera
        self.cam = Camera(self)
        self.rot = Vector2d(0,0)
        #init background
        self.background = Circles()
        #init monkey
        for i in range(MONKEY_LENGTH):
            self.monkey.append(Vector2d(-10,200))
        self.front = 0
        self.wind = Vector2d(0,0)
        self.testprim = primitives.Circle(50,50,0,CIRCLE_SIZE,(255,0,0,1))
        self.wind_line = primitives.Line((0,0),(50,0),color = (255,255,255,1),stroke = 3, rotation = 0)
    def advanceIndex(self):
        if self.front < len(self.monkey)-1:
            self.front += 1
        else: self.front = 0
        return self.front
    def getPrev(self):
        """returns point immedietely behind self.front"""
        if self.front > 0: return self.front - 1
        else: return len(self.monkey)-1
    def update(self,dt):

        #handle input events
        self.handle_joy_event(dt)
        #just in case
        pygame.event.clear()

        #calculate monkey position and direction, advance head index
        cur = self.monkey[self.front]
        dir = self.monkey[self.front] - self.monkey[self.getPrev()]
        index = self.advanceIndex()

        #calculate change and advance head position
        #print self.move*STICK_SPEED + self.wind*WIND_SPEED
        change = dir.getNormal()*SPEED + self.move*STICK_SPEED + self.wind*WIND_SPEED
        #print change.magnitude()
        self.monkey[index] = cur+change
        self.cam.setPos(self.monkey[index] + dir.getNormal()*(1/self.cam.scale)*120)

        #self.cam.setPos(self.monkey[index])

        #draw an ugly line
        windpos = self.cam.center(self.cam.convertPoint(self.monkey[index]))
        self.wind_line.setLoc((windpos.x,windpos.y))
        self.wind_line.rotation = -self.cam.rot*180/math.pi

        #move camera
        self.cam.toRot(self.rot)
        #optional, camera follows monkey
        #self.cam.setRot(dir)
    def handle_joy(self,dt):
        if not self.joy:
            self.move = Vector2d(0,0)
            return
        self.move.x = self.joy.get_axis(0)
        self.move.y = self.joy.get_axis(1)*(-1)
        self.move.rotate(self.cam.rot)
        print "movement vector", self.move
        #self.move.normalize()
        self.rot = Vector2d(self.joy.get_axis(2),self.joy.get_axis(3))

    def handle_joy_event(self,dt):
        """unused, event style joystick handling, slightly more efficient I guess"""
        for e in pygame.event.get():
            if e.type == pygame.JOYBUTTONDOWN:
                pass
                #print e.button
            if e.type == pygame.JOYBUTTONDOWN and e.button == 6:
                self.cam.scale-=0.1
            elif e.type == pygame.JOYBUTTONUP and e.button == 7:
                self.cam.scale+=0.1
            elif e.type == pygame.JOYAXISMOTION:
                #print e.joy, e.axis, e.value
                if e.axis == 0: self.joyv.x = e.value
                elif e.axis == 1: self.joyv.y = -e.value
                elif e.axis == 2 and (e.value != 0 or self.rot.y != 0): self.rot.x = -e.value
                elif e.axis == 3 and (e.value != 0 or self.rot.x != 0): self.rot.y = e.value
        #if we want rotation
        self.move = self.joyv.getNormal().rotate(-self.cam.rot)
    def on_draw(self):
        #clear the screen
        self.clear()
        #draw the background image
        self.background.draw(self.cam)
        #draw monkey list
        indices = pointsinbox(self.monkey,0,self.height,0,self.height)
        for e in range(len(self.monkey)):
        #for e in indices:
            #check if on screen
            if (e - self.front) % FREQ == 0:
                pos = self.cam.convertPoint(self.monkey[e])
                pos = self.cam.center(pos)
                #pos = self.monkey[e]
                self.testprim.setLoc((pos.x,pos.y))
                self.testprim.setWidth(self.cam.scale*CIRCLE_SIZE)
                self.testprim.render()
        #draw current direction

        #draw desired direction
        self.wind_line.render()
    def on_key_press(self,symbol,modifiers):
        #print "key pressed:",symbol,modifiers
        if symbol == window.key.ESCAPE:
            exit()
        if symbol == window.key.F12:
            pass
        if symbol == window.key.SPACE:
            pass
        if symbol in (window.key.LEFT,window.key.RIGHT,window.key.UP,window.key.DOWN):
            if symbol == window.key.LEFT:
                self.wind = Vector2d(-1,0)
            if symbol == window.key.RIGHT:
                self.wind = Vector2d(1,0)
            if symbol == window.key.DOWN:
                self.wind = Vector2d(0,-1)
            if symbol == window.key.UP:
                self.wind = Vector2d(0,1)
            self.wind.rotate(self.cam.rot)

    def on_key_release(self,symbol,modifiers):
        if symbol in (window.key.LEFT,window.key.RIGHT,window.key.UP,window.key.DOWN):
            self.wind = Vector2d(0,0)
        if symbol == window.key.SPACE:
            pass

def run():
    print "now running test application"
    window = Win(width = 800, height = 640)
    pyglet.app.run()

if __name__ == "__main__":
    run()


