import sys
import pyglet
import time
import primitives
import pygame
from pyglet import window
from datatypes import *


MONKEY_LENGTH = 1000
FREQ = 5
SPEED = 2
STICK_SPEED = .4
WIND_SPEED = .5

def pointsinbox(l,x1,x2,y1,y2):
    indices = list()
    for i in range(len(l)-1):
        p = l[i]
        if p.x < x1 or p.x > x2 or p.y < y1 or p.y > y2:
            continue
        indices.append(i)
    return indices
class Camera:
    #this camera class should support rotation and scaling
    def __init__(self,window):
        self.window = window
        self.width = window.width
        self.height = window.height
        self.pos = Vector2d(0,0)
        self.rot = 0
        self.scale = 1
    def setPos(self,pos):
        self.pos = Vector2d(pos.x,pos.y)
        print self.pos
    def setRot(self,v):
        """sets rotation based on a vector assumed to be centerd at self.pos"""
        self.rot = v.getAngle()
        #print self.rot
    def getRot(self):
        return getVector2dFromPolar(1,self.rot)
    def setScale(self,s):
        self.scale = s
    def convertPoint(self,point):
        #translate 2 center
        v = point-self.pos
        a = v.getAngle()+self.rot
        #rotate
        v.x,v.y = v.x*math.cos(a)-v.y*math.sin(a),v.x*math.sin(a)+v.y*math.cos(a)
        v *= self.scale
        return v
    def center(self,point):
        return Vector2d(point.x + self.width/2, point.y + self.height/2)
    def pointsonscreen(self,points,radius):
        pass

class Win( window.Window ):
    monkey = list()
    move = Vector2d(1,0)
    desired = Vector2d(1,0)
    def __init__(self,*args, **kwargs):
        #init pyglet
        window.Window.__init__(self, *args, **kwargs)
        pyglet.clock.set_fps_limit(30)
        pyglet.clock.schedule(self.update)
        #init pygame joystick stuff
        pygame.init()
        self.joy = None
        if pygame.joystick.get_count() > 0:
            self.joy = pygame.joystick.Joystick(0)
            self.joy.init()
        self.joyv = Vector2d(0,0)
        #init camera
        self.cam = Camera(self)
        #init monkey
        for i in range(MONKEY_LENGTH):
            self.monkey.append(Vector2d(-10,200))
        self.front = 0
        self.wind = Vector2d(0,0)
        self.testprim = primitives.Circle(50,50,0,50,(255,0,0,1))
    def advanceIndex(self):
        if self.front < len(self.monkey)-1:
            self.front += 1
        else: self.front = 0
        return self.front
    def getPrev(self):
        """returns point immedietely behind self.front"""
        if self.front > 0: return self.front - 1
        else: return len(self.monkey)-1
    def update(self,dt):
        #additional josytick handilng stuff here"
        rot = Vector2d(0,0)
        for e in pygame.event.get():
            if e.type == pygame.JOYBUTTONDOWN and e.button == 1:
                pass
            elif e.type == pygame.JOYBUTTONUP and e.button == 1:
                print e.button
            elif e.type == pygame.JOYAXISMOTION:
                #print e.joy, e.axis, e.value
                if e.axis == 0: self.joyv.x = e.value
                elif e.axis == 1: self.joyv.y = -e.value
                elif e.axis == 2: rot.x = e.value
                elif e.axis == 3: rot.y = -e.value

        self.move = self.joyv.getNormal()
        if rot.x != 0 or rot.y != 0:
            self.cam.setRot(rot)

        cur = self.monkey[self.front]
        dir = self.monkey[self.front] - self.monkey[self.getPrev()]
        index = self.advanceIndex()
        change = dir.getNormal()*SPEED + self.move*STICK_SPEED + self.wind*WIND_SPEED
        #print change.magnitude()
        self.monkey[index] = cur+change
        self.cam.setPos(self.monkey[index])
    def on_draw(self):
        #clear the screen
        self.clear()
        #draw monkey list
        indices = pointsinbox(self.monkey,0,600,0,480)
        for e in range(len(self.monkey)):
        #for e in indices:
            #check if on screen
            if (e - self.front) % FREQ == 0:
                pos = self.cam.convertPoint(self.monkey[e])
                pos = self.cam.center(pos)
                #pos = self.monkey[e]
                self.testprim.setLoc((pos.x,pos.y))
                self.testprim.render()
        #draw current direction

        #draw desired direction
        pass
    def on_key_press(self,symbol,modifiers):
        #print "key pressed:",symbol,modifiers
        if symbol == window.key.ESCAPE:
            exit()
        if symbol == window.key.F12:
            pass
        if symbol == window.key.SPACE:
            pass
        if symbol == window.key.LEFT:
            self.wind = Vector2d(-1,0)
        if symbol == window.key.RIGHT:
            self.wind = Vector2d(1,0)
        if symbol == window.key.DOWN:
            self.wind = Vector2d(0,-1)
        if symbol == window.key.UP:
            self.wind = Vector2d(0,1)

    def on_key_release(self,symbol,modifiers):
        if symbol == window.key.SPACE:
            pass

def run():
    print "now running test application"
    window = Win()
    pyglet.app.run()

if __name__ == "__main__":
    run()


